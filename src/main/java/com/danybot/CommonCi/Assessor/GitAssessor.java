package com.danybot.CommonCi.Assessor;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.BranchTrackingStatus;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GitAssessor extends Assessor {

	private UsernamePasswordCredentialsProvider credential;
	private String repoName;
	private String repoPath;
	private String branch;
	private Git git;
	private boolean freshClone = false;

	/**
	 * Setting up the credential and project folder. private git project need user
	 * name and password for accessing the source code.
	 * 
	 * @param remoteURL {String}
	 * @param userName  {String}
	 * @param password  {String}
	 */
	public GitAssessor(String remoteURL, String userName, String password) {
		super(remoteURL);
		this.credential = new UsernamePasswordCredentialsProvider(userName, password);

		int startIndex = remoteURL.lastIndexOf("/");
		int endIndex = remoteURL.indexOf(".git");
		if (startIndex < endIndex && endIndex > 0) {
			this.repoName = remoteURL.substring(startIndex + 1, endIndex);
			this.repoPath = "../test/" + repoName + "/.git";
		}

		this.connectGit();
		if (Objects.nonNull(this.git)) {
			try {
				this.branch = this.git.getRepository().getBranch();
			} catch (IOException e) {
				log.error("Fail to load the branch for {}", this.repoName);
			}
		}
	}

	/**
	 * Tries to get the local repository. If there is no local Repository then it
	 * will clone from the remote.
	 */
	private void connectGit() {
		try {
			if (this.sourceExist()) {
				log.info("Opening the git {}", this.repoName);
				this.git = Git.open(new File(this.repoPath));
			} else {
				log.info("Cloning the git {}", this.repoName);
				this.git = Git.cloneRepository().setURI(this.getRemoteURL())
						.setDirectory(new File(this.getLocalRepoPath())).setCredentialsProvider(this.credential).call();
				this.freshClone = true;
			}
			log.info("Fetching the git {}", this.repoName);
			this.git.fetch().setCredentialsProvider(credential).call();
		} catch (Exception e) {
			log.error("Fail to connect {}", e.getMessage());
		}
	}

	private String getLocalRepoPath() {
		return this.repoPath.replace(".git", "");
	}

	@Override
	public void pullSource() {
		try {
			if (this.hasCodeChanged()) {
				log.info("Pulling the git {}", this.repoName);
				git.pull().setCredentialsProvider(credential).call();
			}
		} catch (Exception e) {
			log.error("Fail to pull the source {}", this.repoName);
		}
	}

	@Override
	public boolean sourceExist() {
		File file = new File(this.repoPath);
		return file.exists();
	}

	@Override
	public boolean hasCodeChanged() {
		if (Objects.nonNull(this.git)) {
			try {
				BranchTrackingStatus trackingStatus = BranchTrackingStatus.of(this.git.getRepository(), this.branch);
				return trackingStatus.getBehindCount() != 0;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean isFreshClone() {
		return freshClone;
	}

	public String getRepoName() {
		return repoName;
	}
}
