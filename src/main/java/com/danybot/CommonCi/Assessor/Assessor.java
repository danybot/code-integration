package com.danybot.CommonCi.Assessor;

public abstract class Assessor {

	private String remoteURL = null;

	/**
	 * Initialize with the remoteURL;
	 * 
	 * @param remoteURL {String}
	 */
	public Assessor(String remoteURL) {
		this.remoteURL = remoteURL;
	}

	/**
	 * pull the source code from the remote repository.
	 */
	public abstract void pullSource();

	/**
	 * check whether the source exits or not
	 */
	public abstract boolean sourceExist();
	
	/**
	 * has code changed
	 */
	public abstract boolean hasCodeChanged();

	/**
	 * set the remote URL.
	 * 
	 * @param remoteURL {String}
	 */
	public void setRemoteURL(String remoteURL) {
		this.remoteURL = remoteURL;
	}

	/**
	 * get the remote URL
	 * 
	 * @return {String}
	 */
	public String getRemoteURL() {
		return this.remoteURL;
	}
}
