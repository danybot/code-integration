package com.danybot.CommonCi.Network;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utils {

	/**
	 * return the state of network by scanning all the interfaces. If there is any
	 * successful connection then it will return true or it will return false.
	 * 
	 * @return {boolean}
	 */
	public static boolean isNetworkUp() {
		Enumeration<NetworkInterface> interfaces;
		try {
			interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface interf = interfaces.nextElement();
				if (interf.isUp() && !interf.isLoopback())
					return true;
			}
		} catch (SocketException e) {
			log.error("Intrupt in accessing the Network card ", e.getMessage());
		}
		return false;
	}

	/**
	 * return true if the network is down. It it is up then it will return false.
	 * 
	 * @return {boolean}
	 */
	public static boolean isNetworkDown() {
		return !isNetworkUp();
	}

	/**
	 * handy wrapper for pausing the program. It handles the exceptions so if there
	 * is any error on pausing the program then it will continue the main logic
	 * without terminating the main program.
	 * 
	 * @param milliSecond {long}
	 */
	public static void waitForMilliSecond(long milliSecond) {
		try {
			Thread.sleep(milliSecond);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
