package com.danybot.CommonCi;

import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

import com.danybot.CommonUtils.SystemUtils;
import com.danybot.CommonUtils.SystemUtils.OsType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Engine {

	OutStream errorStream = new OutStream();
	OutStream infoStream = new OutStream();

	/**
	 * Singleton class
	 */
	private Engine() {
	};

	/**
	 * run the script commands one by one.
	 * 
	 * @param commands {List<String>}
	 * @return boolean
	 */
	// TODO make as an wrapper and handle the single process || multiThread process.
	public static boolean runScript(List<String> commands) {
		Engine scriptEngine = new Engine();
		return scriptEngine.runScriptInternal(commands);
	}

	/**
	 * execute the commands one by one. on the console OS dependent for windows it
	 * will be default cmd. If the command cause an error or fail then it will
	 * return false. If it succeed then it will return true.
	 * 
	 * @param commands
	 * @return {boolean}
	 */
	private boolean runScriptInternal(List<String> commands) {
		if (Objects.isNull(commands) || commands.isEmpty()) {
			return true;
		}
		String[] terminal = { SystemUtils.getOsType() == OsType.Windows ? "cmd" : "bash" };
		Process process = null;
		try {
			process = Runtime.getRuntime().exec(terminal);
			new Thread(new SyncPipe(process.getErrorStream(), errorStream)).start();
			new Thread(new SyncPipe(process.getInputStream(), infoStream)).start();
			PrintWriter stdin = new PrintWriter(process.getOutputStream());
			for (String script : commands) {
				stdin.println(script);
			}
			stdin.close();
			process.waitFor();
		} catch (Exception e) {
			log.warn("commond fail ", e.getMessage());
		} finally {
			if (Objects.nonNull(process)) {
				process.destroy();
			}
		}
		return !errorStream.isUtilized();
	}
}
