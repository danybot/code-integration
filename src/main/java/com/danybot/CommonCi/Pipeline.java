package com.danybot.CommonCi;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import com.danybot.CommonCi.Assessor.GitAssessor;
import com.danybot.CommonCi.Entity.PipelineEntity;
import com.danybot.CommonCi.Entity.Module.GitEntity;
import com.danybot.CommonCi.Network.Utils;
import com.danybot.CommonUtils.StringFormatter;
import com.danybot.CommonUtils.YamlConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Pipeline {

	private static final int WAIT_FOR_NETWORK_UP_IN_MILLI = 5000;

	private static final String DEFAULT_FILE_NAME = "pipeline.yml";

	/**
	 * main method to run the pipeline.
	 * 
	 * @param args {String[]}
	 */
	public static void main(String[] args) {
		if (Objects.isNull(args) || args.length == 0) {
			args = new String[] { DEFAULT_FILE_NAME };
		}
		scheduleOnlyIfCodeChange(args);
	}

	/**
	 * schedule all the job only if the code is updated. If we need to run the
	 * pipeline irrespective to the code then you might use alternate method
	 * <code>executePipeline</code>.
	 * 
	 * @param args {String[]}
	 */
	public static void scheduleOnlyIfCodeChange(String[] args) {
		if (Utils.isNetworkDown()) {
			log.info("Network is down. Waining for {} seconds", WAIT_FOR_NETWORK_UP_IN_MILLI);
			Utils.waitForMilliSecond(WAIT_FOR_NETWORK_UP_IN_MILLI);
			if (Utils.isNetworkDown()) {
				log.warn("Terminating, Still network is down, check the network connection");
				return;
			}
		}

		Map<String, PipelineEntity> pipelineEntity = new LinkedHashMap<String, PipelineEntity>();
		for (String fileName : args) {
			YamlConfiguration config = YamlConfiguration.getInstance(fileName);
			for (String pipeline : config.getAllKey()) {
				pipelineEntity.put(pipeline, config.getModel(pipeline, PipelineEntity.class));
			}
		}
		boolean runPiepline = false;
		log.info("Checking for update");
		for (Map.Entry<String, PipelineEntity> pipeline : pipelineEntity.entrySet()) {
			GitEntity gitEntity = pipeline.getValue().getGit();
			if (Objects.nonNull(gitEntity)) {
				GitAssessor gitAssessor = new GitAssessor(gitEntity.getRemoteUrl(), gitEntity.getUserName(),
						gitEntity.getPassword());
				runPiepline = gitAssessor.hasCodeChanged() || gitAssessor.isFreshClone() || runPiepline;
				if (gitAssessor.hasCodeChanged()) {
					gitAssessor.pullSource();
					log.info(StringFormatter.indent("Source for repo {} have been updated", 1),
							gitAssessor.getRepoName());
				}
			}
		}

		if (runPiepline) {
			log.info("Installing the updates");
			executePipeline(pipelineEntity);
		} else {
			log.info("Source is up-to-date");
		}
	}

	/**
	 * execute the pipeline one by one. If <code>onSucceed</code> or
	 * <code>onFail</code> has the <code>break</code> key word then pipeline will be
	 * terminated. By default <code>onFail</code> return the <code>break</code>, if
	 * it need to continue the other pipeline then mention the name.
	 * 
	 * @param pipelineEntity {Map<String, PipelineEntity>}
	 */
	private static void executePipeline(Map<String, PipelineEntity> pipelineEntity) {
		boolean continuePipeline = true;
		String nextJob = "";
		for (Map.Entry<String, PipelineEntity> pipeline : pipelineEntity.entrySet()) {
			if (nextJob.isEmpty() || nextJob.equals(pipeline.getKey())) {
				PipelineEntity entity = pipeline.getValue();
				log.info("Running pipeline {}", pipeline.getKey());
				continuePipeline = Engine.runScript(entity.getScript());
				nextJob = continuePipeline ? entity.getOnSucceed().equals("continue") ? "" : entity.getOnSucceed()
						: entity.getOnFail();
				if (nextJob.equals("break")) {
					break;
				}
			}

		}
	}
}
