package com.danybot.CommonCi;

import java.io.InputStream;
import java.io.OutputStream;

class SyncPipe implements Runnable {
	private final OutputStream outputStream;
	private final InputStream inputStream;

	/**
	 * SyncPipe make an virtual connection between the input and output stream. It
	 * runs on the tread.
	 * 
	 * @param inputStream  {InputStream}
	 * @param outputStream {OutputStream}
	 */
	public SyncPipe(InputStream inputStream, OutputStream outputStream) {
		this.inputStream = inputStream;
		this.outputStream = outputStream;
	}

	/**
	 * Runnable that runs on the tread to listen the in stream and write to the out
	 * stream
	 */
	public void run() {
		try {
			final byte[] buffer = new byte[1024];
			for (int length = 0; (length = inputStream.read(buffer)) != -1;) {
				outputStream.write(buffer, 0, length);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
