package com.danybot.CommonCi;

import java.io.IOException;
import java.io.OutputStream;

public class OutStream extends OutputStream {

	private boolean isUtilized = false;

	@Override
	public void write(int b) throws IOException {
		System.err.write(b);
		isUtilized = true;
	}

	/**
	 * return if the OutputStream is utilized.
	 * 
	 * @return {boolean}
	 */
	public boolean isUtilized() {
		return this.isUtilized;
	}
}
