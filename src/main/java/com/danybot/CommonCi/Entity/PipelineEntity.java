package com.danybot.CommonCi.Entity;

import com.danybot.CommonCi.Entity.Module.BaseEntity;
import com.danybot.CommonCi.Entity.Module.GitEntity;

public class PipelineEntity extends BaseEntity {

	private GitEntity git;

	public GitEntity getGit() {
		return git;
	}

	public void setGit(GitEntity git) {
		this.git = git;
	}
}
