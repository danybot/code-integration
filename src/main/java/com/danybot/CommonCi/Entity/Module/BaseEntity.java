package com.danybot.CommonCi.Entity.Module;

import java.util.List;
import java.util.Objects;

public class BaseEntity {

	private List<String> script;

	private String onSucceed;

	private String onFail;

	public List<String> getScript() {
		return script;
	}

	public void setScript(List<String> script) {
		this.script = script;
	}

	public String getOnSucceed() {
		return Objects.isNull(this.onSucceed ) ? "continue" : this.onSucceed;
	}

	public void setOnSucceed(String onSucceed) {
		this.onSucceed = onSucceed;
	}

	public String getOnFail() {
		return Objects.isNull(this.onFail) ? "break" : this.onFail;
	}

	public void setOnFail(String onFail) {
		this.onFail = onFail;
	}
}
